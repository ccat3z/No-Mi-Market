package cc.c0ldcat.fxxkmimarket;

import de.robv.android.xposed.*;
import de.robv.android.xposed.callbacks.XC_LoadPackage;

import java.util.List;

public class Hooker implements IXposedHookLoadPackage {
    @Override
    public void handleLoadPackage(XC_LoadPackage.LoadPackageParam loadPackageParam) throws Throwable {
        if (loadPackageParam.packageName.equals("android")) {
            XposedHelpers.findAndHookMethod("com.android.server.pm.PackageManagerServiceInjector",
                    loadPackageParam.classLoader,
                    "getMarketResolveInfo",
                    List.class,
                    XC_MethodReplacement.returnConstant(null));

            XposedBridge.log("hook package manager service injector");
        }
    }
}